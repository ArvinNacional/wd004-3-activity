// array --- is a collection of related data. Denoted by []


const fruits = ['Apple', 'Banana', 'Kiwi'];

const ages = [1,2,3,4];

const students = [ 
	{name: "brandon", age: 11}, 
	{name: "Brenda", age: 12},
	{name: "Celmer", age:15},
	{name: "Archie", age: 16},

]


