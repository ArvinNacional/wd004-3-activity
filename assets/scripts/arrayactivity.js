const pila = [];

function showAll() {
	return pila;
}

function addQueue(name) {
	const names = name;
	pila.push(names);

	return "Successfully added " + names;

}


function deQueue() {
	pila.shift();

	return "Successfully shifted the first on the pila";

}

function expedite(index) {
	pila.splice(index,1);

	return "Successfully removed a person from the pila";
}


function reverseDequeue() {
	pila.pop();

	return "Successfully removed the last person on the pila";
}

function addVIP(vip) {
	const daVIP = vip;
	pila.unshift(daVIP);

	return "Successfully added a vIP on the line";

}

function updateName(index, newName) {
	pila[index] = newName;
	return "Successfully updated a name on the pila.";
}

